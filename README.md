# Prerequisite 
1. Docker is installed (latest)
2. docker-compose is installed (latest) 

# Setting Up
```
# clone this repo
git clone git@bitbucket.org:sakhunzai/savins.git dkr_savins

# clone savins repo to src/ subdirectory
git clone [savins-repo-url] dkr_savins/src/savins
```

## macOS / Linux
1. Update etc/hosts

```
sudo vim /etc/hosts

#-----  Docker-SAVINS  -----------
127.0.0.1 savins.dkr
127.0.0.1 dev.savins.dkr
```

2. Start docker services
```
  cd dkr_savins
  ./docker.sh
```

3. Access individual container

```
# Access web container
docker exec -it dkr_savins_web_1 bash

# Access db container
docker exec -it dkr_savins_db_1 bash

# Access nginx container
docker exec -it dkr_savins_nginx_1 bash

```

4. View Logs
```
docker logs dkr_savins_web_1
docker logs dkr_savins_nginx_1
docker logs dkr_savins_db_1
```

## Stop services

```
  docker-compose down
```


# Architecture
## Overview
![IMAGE](assets/images/docker_arc.png)

## Request Flow
![IMAGE](assets/images/request_flow.png)

## Service Interaction
![IMAGE](assets/images/interaction.png)



# Caveats

1. nginx container publishes port 80 and binds on host port 80 therefore, no service should be running on 80 on host machine
2. current django `makemigrations` command fails due to some circular dependency
   

# TODO
Add a single script to manage docker services e.g

`./docker.sh -k start|stop|build`

Container interaction

`./docker.sh -s web|db|nginx`