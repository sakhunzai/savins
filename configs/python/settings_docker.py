from insurecert.settings_debug import *
import os

# Uncomment when we go live
ADMIN_HOSTS = [
            'savins.dkr',
]

DATABASES['default']['USER'] = 'postgres'

DATABASES['default']['HOST'] = 'db'

COMPRESS_VERBOSE=True

COMPRESS_ENABLED=True

DEBUG=False

COMPRESS_PRECOMPILERS = (
   ('text/less', '%s/node_modules/.bin/lessc --compress --include-path="%s" {infile} {outfile}' % (PROJECT_ROOT,PROJECT_ROOT)),
)

#write logs  to file as well as console
SAVINS_LOG_LEVEL = os.environ.get('SAVINS_LOG_LEVEL', 'WARN')
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'file': {
            'level': SAVINS_LOG_LEVEL,
            'class': 'logging.FileHandler',
            'filename': '/var/log/savins/savins.log',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'django': {
            'handlers':['file'],
            'propagate': True,
            'level':SAVINS_LOG_LEVEL,
        },
        'MYAPP': {
            'handlers': ['file'],
            'level': SAVINS_LOG_LEVEL,
        },
    }
}