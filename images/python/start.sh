#!/bin/bash

RETRIES=5
PG_HOST=db
PG_USER=postgres
PG_DATABASE=insurecertdevel

npm install

DJANGO_SETTINGS_MODULE='settings_docker'

until psql -h $PG_HOST -U $PG_USER -d $PG_DATABASE -c "select 1" > /dev/null 2>&1 || [ $RETRIES -eq 0 ]; do
  echo "Waiting for postgres server, $((RETRIES--)) remaining attempts..."
  sleep 1
done

#command: bash -c "python manage.py makemigrations && python manage.py migrate && gunicorn insurecert.wsgi -b 0.0.0.0:8000"
python manage.py migrate --settings=${DJANGO_SETTINGS_MODULE}  && gunicorn --env DJANGO_SETTINGS_MODULE=${DJANGO_SETTINGS_MODULE} insurecert.wsgi  --log-level=info -b 0.0.0.0:8000 --reload
